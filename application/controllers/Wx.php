<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Wx extends CI_Controller {
    
    /**
     * 微信登录地址
     * @var string 
     */
    protected $wx_address;
    
    /**
     * 微信登录的用户数组
     * @var array 
     */
    protected $user;
    
    /**
     * 微信登录的用户名
     * @var type 
     */
    protected $username;

    public function __construct() {
        parent::__construct();
        $this->qq = new stdClass();
        $this->load->helper('cli');
        $this->load->library('WebApi');
    }
    
    public function index() {
        //欢迎信息
        $this->_hello();
        
        //登录二维码
        $this->_qrcode();
        
        
    }
    
    /**
     * 欢迎信息
     */
    protected function _hello()
    {
        \Helper\CLI::echo_system('***********************************');
        \Helper\CLI::echo_system('*****欢迎来到七七终端版115********');
        \Helper\CLI::echo_system('***********************************');
    }
    
    /**
     * 获取登录二维码
     */
    protected function _qrcode() {
        $this->load->database();
        //访问下首页 并且创建cookie文件
        $this->webapi->get_index();
        $get_login_status_num = 0;
        $login = false;
        var_dump($this->webapi->get_token());
        
        \Helper\CLI::echo_warning('开始登录,获取二维码,这可能需要五六秒的时间,请耐心等待');
        $this->webapi->get_qrcode();
        
        while (true) {
            $get_login_status =  $this->webapi->get_login_status();
            $im =  $this->webapi->get_im();
            if(0 == $get_login_status->data->status)//超过十次就跳出 继续获取一个新的二维码
            {
                \Helper\CLI::echo_system('等待扫码');
            }
            else if(1 == $get_login_status->data->status)
            {
                \Helper\CLI::echo_system($get_login_status->data->status.':'.$get_login_status->data->msg);
            }
            else if(2 == $get_login_status->data->status)
            {
                \Helper\CLI::echo_system('登录成功');
                break;
            }
            else
            {
                \Helper\CLI::echo_system($get_login_status->data->status.':'.$get_login_status->data->msg);
            }
        }
        $this->webapi->get_bridge_assets();
        $this->webapi->login();
        $this->webapi->ajax_login();
        $this->webapi->get_my();
        $this->webapi->get_q_api_home();
        $this->webapi->get_user_session_id();
        
        $this->webapi->didi();//循环这个
        
        $file = $this->webapi->file_list();
        $file_list = [];
        foreach($file->data as $item)
        {
            $file_list[$item->n] = $item;
        }
        echo $file_list['离线下载']->cid;
        $file = $this->webapi->file_list($file_list['离线下载']->cid);
        foreach($file->data as $item)
        {
            \Helper\CLI::echo_system($item->n);
        }
        
        
        $this->webapi->get_user_id();
        while(true)
        {
            $this->webapi->didi();
            $this->db->reconnect();
            $sql = "select * from bot_add_seed where status = 0";
            $query = $this->db->query($sql);

            foreach ($query->result() as $row)
            {
                \Helper\CLI::echo_system($this->webapi->add_task($row->data));
                $this->db->query("update bot_add_seed set status = 1 where id = {$row->id}");
                sleep(5);
            }
            sleep(5);
        }
        
    }
    
    /**
     * 登录115
     */
    protected function _login() {
        \Helper\CLI::echo_system('扫码成功,开始登录微信');
        $success_code = $this->webapi->get_login_success_code($this->wx_address);
        if (!isset($success_code['ret'])) {
            exit('login error');
        }
        \Helper\CLI::echo_system('微信登录成功,开始初始化信息');
        $this->webapi->get_wxinit();
        \Helper\CLI::echo_system('初始化成功,开始获取好友列表');
        $this->webapi->get_friend();
        \Helper\CLI::echo_system('好友列表获取成功,开始读取用户信息');
        $this->user = $this->webapi->get_user();
        $this->username = isset($this->user['NickName'])?$this->user['NickName']:'未知';
        \Helper\CLI::echos('登录用户:'.$this->username);
        //$this->webapi->get_wx_status_notify();
    }
    
    
    /**
     * 生成linux 终端下的二维码
     */
    protected function _terminal_qrcode($qrcode)
    {
        if(!is_array($qrcode))
        {
            return false;
        }
        
        $line_count = count($qrcode);
        $strlen = strlen($qrcode[0])+2;
        $top = str_repeat(0,$strlen);
        array_unshift($qrcode,$top);
        $qrcode[] = $top;
        
        foreach($qrcode as $item)
        {
            $item = "0{$item}0";
            for($i=1;$i<=$strlen;$i++)
            {
                $len = $i-1;
                if(0 == $item{$len})
                {
                    \Helper\CLI::echo_colore('  ',"red", "yellow",false);
                }
                else
                {
                    \Helper\CLI::echo_colore('  ',"red", "black",false);
                }
            }
            echo PHP_EOL;
        }
    }

}
