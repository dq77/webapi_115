<?php
namespace WebApi\WebApi115;
use \WebApi\WebApi115\WebApiHelper;
use \WebApi\WebApi115\WebApiCurl;
/*****************************************************************************************
 * 模拟微信网页扫码登录代码 
 *****************************************************************************************
 */

/**
 * 微信模拟登录
 * @email 397109515@qq.com
 * @author  seven
 */
class WebApi {

    /**
     * cookie文件地址
     * @var type 
     */
    private $cookie_file;
    
    /**
     * 存储登录成功后的数据
     * @var arr 
     */
    private $login_success_data;
    
    /**
     * 保存synckey 信息  这里是url里用的  是一个字符串
     * @var string 
     */
    private $synckey;
    
    /**
     * 保存synckey信息 这里是post的时候用的  是一个数组
     * @var array
     */
    private $SyncKeyArr;
    
    /**
     * 二维码存放目录
     * @var type 
     */
    private $qrcode_path = 'qrcode/';
    
    /**
     * cookie存放目录
     * @var type 
     */
    private $cookie_path = 'cookie/';
    
    
    /**
     * @var Curl对象 $curl
     */
    protected $curl;
    
    /**
     * token信息
     */
    protected $token;

    protected $bridge_assets;
    protected $user;
    /**
     * 构造方法
     * @param string $cookie_path
     */
    public function __construct($cookie_path = false)
    {
        $this->client_msg_id = time().rand(100,999);
        $this->curl = new \WebApi\WebApi115\WebApiCurl($cookie_path);
    }
    
    
    /**
     * 设置cookie存放目录
     * @param type $path
     */
    public function set_cookie_path($path)
    {
        $this->curl->set_cookie_path($path);
    }
    
    /**
     * 设置二维码存放目录
     * @param type $path
     */
    public function set_qrcode_path($path)
    {
        if(!is_dir($path))
        {
            mkdir($path, '0777', true);
        }
        $this->qrcode_path = $path;
    }
    
    /**
     * 根据群id 获取群的名称 主要用于判断消息是不是来自群
     * @param type $group_id
     * @return type
     */
    public function get_group_name($group_id)
    {
        if(strpos($group_id, '@@') === 0)
        {
            return isset($this->group_id[$group_id])?$this->group_id[$group_id]:'未知';
        }
        return false;
    }
    
    /**
     * 读取下首页 天知道首页是不是有啥必要的cookie
     */
    public function get_index() {
        $url = 'http://115.com/';
        $this->curl->set_cookie_file_path(time());
        $this->curl->curl_get($url);
    }
    
    
    /**
     * 获取二维码参数
     * @return string
     */
    public function get_qrcode() {
        //http://115.com/scan/dg-97f80f5fbfbffeec4e795285a78e3e83d744c339
        $time = time();
        $url = "http://qrcodeapi.115.com/api/1.0/web/1.0/qrcode?qrfrom=1&&uid={$this->token->data->uid}&_{$time}&_t={$time}";
        $file = $this->qrcode_path.time().'.png';
        file_put_contents($file, $this->curl->curl_get($url));
        return $file;
    }
    
    /**
     * 获取token
     */
    public function get_token()
    {
       
        $url = "http://qrcodeapi.115.com/api/1.0/web/1.0/token";
        $data = $this->curl->curl_get($url);
        return $this->token = json_decode($data);
    }
    
    /**
     * 获取登陆状态
     */
    public function get_login_status()
    {
        $time = time();
        echo $url = "http://qrcodeapi.115.com/get/status/?uid={$this->token->data->uid}&sign={$this->token->data->sign}&time={$this->token->data->time}&_={$time}";
        $data = $this->curl->curl_get($url);
        return json_decode($data);
    }
    
    /**
     * 获取状态
     */
    public function get_im()
    {
        echo $url = "http://msg.115.com/proapi/im.php?ac=signin";
        $data = $this->curl->curl_get($url);
        return json_decode($data);
    }
    
        /**
     * 获取首页信息
     */
    public function get_my()
    {
        $time = time();
        $url = "http://my.115.com/?ct=guide&ac=status&_t={$time}";
        $data = $this->curl->curl_get($url);
        return json_decode($data);
    }
        
    /**
     * 获取首页信息
     */
    public function get_q_api_home()
    {
        $url = "http://q.115.com/api/q_api_home.php";
        $data = $this->curl->post($url,"api_method=get_my_q");
        return json_decode($data);
    }
    
    /**
     * 登录
     */
    public function ajax_login()
    {
        $time = time();
        $url = "http://passport.115.com/?ct=ajax&ac=islogin&is_ssl=1&_={$time}";
        $time = $time + 2;
        $header = [
                    "{$this->bridge_assets}",
                    "User-Agent:Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36",
                    "X-Requested-With:XMLHttpRequest",
                    "Host:passport.115.com"
                  ];
        $data = $this->curl->curl_get($url,true,$header,"http://passport.115.com/bridge_assets.html?cb_key=iframe_cb_key__{$time}_2&_{$time}");
        
        return json_decode($data);
    }
    
    /**
     * 登录
     */
    public function login()
    {
        $time = time();
        $url = "http://passport.115.com/?ct=login&ac=qrcode&key={$this->token->data->uid}&v=&goto=http%3A%2F%2Fpassport.115.com%2F%3Fct%3Dlogin%26ac%3Dempty_page%26is_ssl%3D1";
        $time = $time + 2;
        $data = $this->curl->curl_get($url);
        
        return $data;
    }
    
    
    /**
     * 获取首页信息
     */
    public function get_bridge_assets()
    {
        $time = time();
        $this->bridge_assets = "http://passport.115.com/bridge_assets.html?cb_key=iframe_cb_key__{$time}_2&_t{$time}";
        $data = $this->curl->curl_get($this->bridge_assets);
        return json_decode($data);
    }
    
    /**
     * 获取用户session等信息 后面滴滴有用
     * @return type
     */
    public function get_user_session_id()
    {
        $url = "http://msg.115.com/proapi/im.php?ac=signin";
        $data = $this->curl->curl_get($url);
        return $this->user = json_decode($data);
    }
    
    
    /**
     * 发送滴滴消息 这个不知道是干嘛的
     */
    public function didi()
    {
        $time = time();
        $url = "http://im30.115.com/chat/r?VER=2&c=b3&s={$this->user->session_id}&_t={$time}";
        $this->curl->curl_get($url);
    }
    
    /**
     * 读取文件列表
     */
    public function file_list($cid = 0,$aid = 0,$offset = 0,$limit=40)
    {
        $url = "http://web.api.115.com/files?aid={$aid}&cid={$cid}&o=user_ptime&asc=0&offset={$offset}&show_dir=1&limit={$limit}&code=&scid=&snap=0&natsort=1&source=&format=json";
        $data = $this->curl->curl_get($url);
        return json_decode($data);
    }
    
    /**
     * 添加离线任务
     */
    public function add_task($add_url)
    {
        $time = time();
        $url = "http://115.com/web/lixian/?ct=lixian&ac=add_task_url";
        $data = $this->curl->post($url,"url={$add_url}&uid={$this->uid}&sign={$this->token->data->sign}&time={$time}");
        return json_decode($data);
    }
    
    
    protected $uid;
    /**
     * 获取用户id
     */
    public function get_user_id()
    {
        $lines = file($this->curl->get_cookie_path());  
        $trows = '';

        foreach($lines as $line) {  

            if($line[0] != '#' && substr_count($line, "\t") == 6) {  

                $tokens = explode("\t", $line);  

                $tokens = array_map('trim', $tokens);  

                $tokens[4] = date('Y-m-d h:i:s', $tokens[4]);  

            }  
        }
        return $this->uid = $tokens[6];
    }
    
}
